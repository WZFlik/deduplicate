package config

import (
	"deduplicate/util"
	"encoding/json"
	"fmt"
	"log"
	"os"
)

var Conf *config
type config struct {
	DataFilePath          string `json:"data_file_path"`      //要去重的数据文件路径
	ResultFilePath        string `json:"result_path"`         //生成的结果文件路径
	SplitCount            int    `json:"split_count"`         //文件分割数
	MergeThreadCount      int    `json:"merge_thread_count"`  //一次运行多少个协程来去重合并
	MergeReadWriteBufSize int    `json:"merge_read_write_buf_size"` //文件分割io.reader/writer的缓存区大小
	SplitReadWriteBufSize int    `json:"split_read_write_buf_size"` //文件合并去重io.reader/writer的缓存区大小
	StatPrintInterval     int    `json:"stat_print_interval"` //状态打印时间间隔 小于0时关闭
	MergeMode             int    `json:"merge_mode"`          //文件合并模式 0、1 0：一个协程读取chan中的数据逐行写入，1：使用io.copy复制合并
	SplitMode             int   `json:"split_mode"`//分割模式 1 chan分割 0 routing分割
	SplitChanSize         int    `json:"split_chan_size"`    //分割文件时每个文件对应协程的容量
	DoneFileChanSize4MergeMode1 int `json:"done_file_chan_size_4_merge_mode_1"` //文件写入temp完成chan的容量 mergeMode=1时有效
	MergeChanSize4MergeMode0 int `json:"merge_chan_size_4_merge_mode_0"` //MergeMode == 0时有用 调节bug的大小
}
//读取配置文件中的配置
func init() {
	var filePath = "../config/config.json"
	var file *os.File
	var retry = 0
	var err error
	if Conf == nil {
		Conf = new(config)
	}
	if err != nil {
		util.Log(err)
	}
	//用户输入配置文件目录
	if len(os.Args) > 1 {
		filePath = os.Args[1]
		file, err = os.Open(filePath)
	}
	if err != nil || file == nil {
		//使用goland运行是当前路径在根目录
	tag:
		{
			file, err = os.Open(filePath)
			if retry+=1;retry >3{
				log.Fatal("config read file")
			}
		}
		if err != nil {
			if os.IsNotExist(err) {
				//使用go build\run命令运行时当前目录在main文件目录
				filePath = "./config/config.json"
				file, err = os.Open(filePath)
			}
			if os.IsNotExist(err) {
				log.Println(err)
			}
		}
		if file != nil {
			decoder := json.NewDecoder(file)
			err = decoder.Decode(Conf)
		}
		if err != nil {
			log.Printf("%s:%v\n", "decode config error", err)
			fmt.Println("请输入配置文件:")
			fmt.Scanln(&filePath)
			goto tag
		}
	}
}
