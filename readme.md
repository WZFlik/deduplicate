# 程序描述：超大文件去重
 超大文件去重：给定一个超大（内存一次性放不下）的txt文件，文件内每一行都有一个字符串,字符串中不包含空格，要求对该文件内的字符串去重，并生成去重过后的文件。要求内存使用尽量少，去重时间尽量少。
# 数据生成程序：
内有一个生成数据的文件，运行gen_data.go 生成的数据会方案当前目录的data下面，默认生成2kw行，大概2个G,gen_data只可修改源码来修改生成的数据量
 # 配置项（config.json）说明：
 ```
DataFilePath          string `json:"data_file_path"`      //要去重的数据文件路径
ResultFilePath        string `json:"result_path"`         //生成的结果文件路径`
SplitCount            int    `json:"split_count"`         //文件分割数
MergeThreadCount      int    `json:"merge_thread_count"`  //一次运行多少个协程来去重合并
MergeReadWriteBufSize int    `json:"merge_read_write_buf_size"` //文件分割io.reader/writer的缓存区大小
SplitReadWriteBufSize int    `json:"split_read_write_buf_size"` //文件合并去重io.reader/writer的缓存区大小
StatPrintInterval     int    `json:"stat_print_interval"` //状态打印时间间隔 小于0时关闭
MergeMode             int    `json:"merge_mode"`          //文件合并模式 0、1 0：一个协程读取chan中的数据逐行写入，1：使用io.copy复制合并
SplitMode             int   `json:"split_mode"`//分割模式 1 chan分割 0 routing分割
SplitChanSize         int    `json:"split_chan_size"`    //分割文件时每个文件对应协程的容量
DoneFileChanSize4MergeMode1 int `json:"done_file_chan_size_4_merge_mode_1"` //文件写入temp完成chan的容量 mergeMode=1时有效
MergeChanSize4MergeMode0 int `json:"merge_chan_size_4_merge_mode_0"` //MergeMode == 0时有用 调节bug的大小