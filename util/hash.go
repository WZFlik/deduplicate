package util

import "hash/fnv"

func HashString(v string) uint32 {
	hashcode := uint32(0)
	for i := 0; i < len(v); i++ {
		hashcode = hashcode*uint32(31) + uint32(v[i])
	}
	return hashcode
}

func Hash1(b []byte) uint32{
	a := fnv.New32a()
	_, err := a.Write(b)
	if err != nil {
		Log(err)
	}
	return a.Sum32()

}
